#!/bin/bash -eu
set -euo pipefail

usage() {
	cat <<EOF

**************************************************************
Weather Helper Script
Gets weather in current location and in HK
For more support - http://wttr.in/:help

Usage: ./weather.sh 
**************************************************************

EOF
}

main() {
  curl wttr.in?u?1
  curl wttr.in/Hong+Kong?u?1
}

main
