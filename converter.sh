#!/bin/bash -eu
set -euo pipefail

declare -r LB_TO_KG="0.453592"
declare -r KG_TO_LB="2.2046"
declare -r F_TO_C="0.5556"	# 5/9
declare -r C_TO_F="1.8" 	# 9/5


usage() {
	cat <<EOF

**************************************************************
Converter Helper Script
Converts unit(s) of imperial measurement to unit(s) of metric measurement or 
vice versa

Usage: ./converter.sh [-t <type>] [-q <quantity>]
  Options:
  -t <lb/kg/f/c>
  	 type of imperial or metric measurement
  -q <amount>
  	 the quantity to convert
**************************************************************

EOF
}

convert() {
	local type="${1}"
	local quantity="${2}"

	# TODO - AKO-002 - add rounding functionality up to 3rd decimal place

	# Converts pounds (lb) to kilograms (kg) or vice versa
	if [[ "${type}" == "lb" ]]; then
	  local result=$(echo "${quantity} * ${LB_TO_KG}" | bc)
	  echo "${quantity}""${type}" "=" "${result}""kg"
	elif [[ "${type}" == "kg" ]]; then
	  local result=$(echo "${quantity} * ${KG_TO_LB}" | bc)
	  echo "${quantity}""${type}" "=" "${result}""lb"
	fi 

	# Converts farenheit (f) to celcius (c) or vice versa
	if [[ "${type}" == "f" ]]; then
	  local result=$(echo "(${quantity} - 32) * ${F_TO_C}" | bc)
	  echo "${quantity}""${type}" "=" "${result}""c"
	elif [[ "${type}" == "c" ]]; then
	  local result=$(echo "(${quantity} * ${C_TO_F}) + 32" | bc)
	  echo "${quantity}""${type}" "=" "${result}""f"
	fi 
}

main() {
  # TODO - AKO-001 - check if bc (command line calc) is installed
  local type=""
  local quantity=""

  while getopts ":t:q:h" opt; do
    case ${opt} in
      t) # process option t
 	    type="${OPTARG}"
      ;; # process option q
      q)
	    quantity="${OPTARG}"
	  ;;
      h) # process option h
	    usage
	    return 1
      ;;
      \?) 
	     usage
	     return 1
      ;;
    esac
  done	
  shift $((OPTIND -1))

  convert "${type}" "${quantity}"
}

# Check if provided arguments are 0
if [ "${#}" -eq 0 ]; then
  usage
  exit 1
fi

main "${@}"